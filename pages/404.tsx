import classes from './index.module.scss'

//** components
import Link from 'next/link'
import {MainLayout} from '../layout/MainLayout'
import {Container} from '../layout/_components/Container'

export default function ErrorPage() {
  return (
    <MainLayout title="404 not found">
      <Container>
        <section className={classes.error}>
          <h1 className={classes.errorTitle}>Error 404</h1>
          <p className={classes.errorText}>
            Go back to
            <Link href="/">
              <a className={classes.errorLink}> Home page</a>
            </Link>
          </p>
        </section>
      </Container>
    </MainLayout>
  )
}
