import {ROUTES} from '../../constants'
import classes from './index.module.scss'

//** components
import {MainLayout} from '../../layout/MainLayout'
import {WizardContainer} from '../../layout/_components/WizardContainer'
import {Tabs} from '../../layout/Tabs'
import {AuthForm} from '../../layout/AuthForm'
import Link from 'next/link'

export default function SignUp() {

  return (
    <MainLayout title="Sign-up | G-score">
      <WizardContainer>
        <Tabs />
        <div className={classes.formContainer}>
          <h1>Create account</h1>
          <span className={classes.notice}>
            You need to enter your name and email. We will send you a temporary password by email
          </span>
          <AuthForm isSignUp={true} />
        </div>
        <span className={classes.postscript}>Have an account?  </span>
        <Link href={ROUTES.SIGN_IN}>
          <a className={classes.postscript}>Go to the next step</a>
        </Link>
      </WizardContainer>
    </MainLayout>
  )
}

export async function getStaticProps() {
  return {
    props: {},
  }
}
