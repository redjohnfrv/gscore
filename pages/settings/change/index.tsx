import {useState} from 'react'
import {useSelector} from 'react-redux'
import {useRouter} from 'next/router'
import classes from './index.module.scss'
import {ROUTES} from '../../../constants'

//** components
import {MainLayout} from '../../../layout/MainLayout'
import {PersonalForm} from '../../../layout/PersonalForm'
import {PasswordForm} from '../../../layout/PasswordForm'
import {Redirecting} from '../../../layout/Redirecting'

//** utils
import cx from 'classnames'
import {userSelectors} from '../../../redux/user'

export default function Change() {

  const isAuth = useSelector(userSelectors.isAuth)
  const user = useSelector(userSelectors.getUserData)

  const router = useRouter()
  const {push} = router

  const [active, setActive] = useState('info')
  const isPersonal = active === 'info'

  if (!isAuth) {
    push(ROUTES.ROOT)
    return <Redirecting />
  }

  return (
    <MainLayout title="Change settings">
      <div className={classes.settings}>
        <h1 className={classes.title}>Settings</h1>

        <nav className={classes.tabs}>
          <div
            onClick={() => setActive('info')}
            className={cx(classes.tab, {
              [classes.tabActive]: isPersonal
            })}
          >Personal info</div>
          <div
            onClick={() => setActive('password')}
            className={cx(classes.tab, {
              [classes.tabActive]: !isPersonal
            })}
          >Change password</div>
          <div className={cx(classes.activeLine, {
            [classes.activeLineMoved]: !isPersonal
          })} />
        </nav>

        <h2 className={classes.subTitle}>{isPersonal ? 'Personal info' : 'Change password'}</h2>

        {isPersonal
          ? <PersonalForm userInfo={user} />
          : <PasswordForm />
        }
      </div>
    </MainLayout>
  )
}
