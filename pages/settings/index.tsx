import {useSelector} from 'react-redux'
import {useRouter} from 'next/router'
import classes from './index.module.scss'

//** components
import {MainLayout} from '../../layout/MainLayout'
import {Redirecting} from '../../layout/Redirecting'
import {Button} from '../../layout/_components/Button'

//** utils
import {userSelectors} from '../../redux/user'
import {ROUTES} from '../../constants'
import {emailHiding} from '../../helpers'

export default function Settings() {

  const router = useRouter()
  const {push} = router
  const isAuth = useSelector(userSelectors.isAuth)
  const user = useSelector(userSelectors.getUserData)
  const {username, email} = user

  if (!isAuth) {
    push(ROUTES.ROOT)
    return <Redirecting />
  }

  emailHiding(email)

  return (
    <MainLayout title="Personal settings">
      <div className={classes.settings}>
        <h1 className={classes.title}>Settings</h1>
        <div className={classes.info}>
          <div className={classes.blocks}>
            <div className={classes.infoBlock}>
              <span className={classes.caption}>User name: </span>
              <span className={classes.value}>{username}</span>
            </div>
            <div className={classes.infoBlock}>
              <span className={classes.caption}>User email: </span>
              <span className={classes.value}>{emailHiding(email)}</span>
            </div>
          </div>
          <Button
            title="Change info"
            theme="red"
            cWidth="medium"
            onClick={() => push(ROUTES.SETTINGS_CHANGE)}
          />
        </div>
      </div>
    </MainLayout>
  )
}
