import {useEffect} from 'react'
import {useSelector} from 'react-redux'
import {ICard} from '../dto/card'
import {cardsMoq, ROUTES} from '../constants'
import classes from './index.module.scss'

//** components
import {MainLayout} from '../layout/MainLayout'
import {Card} from '../layout/Card'
import Link from 'next/link'

//** utils
import {userSelectors} from '../redux/user'
import {cardsApi} from '../redux/cards/api'
import {toast} from 'react-toastify'
import {cardsSelectors} from '../redux/cards'

interface Props {
  initProducts: ICard[]
  error?: string
}

export default function Index({initProducts, error = ''}: Props) {

  const isAuth = useSelector(userSelectors.isAuth)
  const selectedProduct = useSelector(cardsSelectors.selectedCard)
  const filteredProducts = initProducts.filter(item => item.id !== selectedProduct?.id)

  useEffect(() => {
    if (error) toast.error('Connection error ...')
  }, [error])

  return (
    <MainLayout
      title="Welcome to the G-Score"
    >
      <h1 className={classes.title}>Get started with Gscore today!</h1>
      <section className={classes.cards}>
        {localStorage.getItem('upgrading')
          ? filteredProducts
            .map(item => {
              return (
                <Card
                  key={item.id}
                  id={item.id}
                  sitesCount={item.sitesCount}
                  name={item.name}
                  prices={item.prices}
                  auth={isAuth}
                />
              )
            })
          : initProducts
            .map(item => {
              return (
                <Card
                  key={item.id}
                  id={item.id}
                  sitesCount={item.sitesCount}
                  name={item.name}
                  prices={item.prices}
                  auth={isAuth}
                />
              )
          })
        }
      </section>
      <span className={classes.postscript}>Have more than 10 sites?</span>
      <Link href={ROUTES.ROOT}>
        <a target="_blank" className={classes.postscript}>Contact us</a>
      </Link>
    </MainLayout>
  )
}

export async function getServerSideProps() {
  try {
    const response = await cardsApi.getCards()
    let initProducts: ICard[] = await response.data
    return {
      props: {
        initProducts,
      }
    }
  }
  catch (err: any) {
    return {
      props: {
        initProducts: cardsMoq,
        error: 'Connection error ...',
      }
    }
  }
}
