import {Fragment, useCallback, useEffect, useRef, useState} from 'react'
import {useSelector} from 'react-redux'
import {useRouter} from 'next/router'
import classes from './index.module.scss'
import {ISubscribeItem, ICode} from '../../dto/subscribe'
import {ROUTES, STATUSES} from '../../constants'

//** components
import {MainLayout} from '../../layout/MainLayout'
import {Button} from '../../layout/_components/Button'
import {Sub} from '../../layout/Subs/Sub'
import {Pagination} from '../../layout/Subs/Pagination'
import {License} from '../../layout/Subs/License'
import {Redirecting} from '../../layout/Redirecting'
import {NoSubs} from '../../layout/Subs/noSubs'
import {HollowSub} from '../../layout/Subs/HollowSub'
import {View} from '../../layout/Subs/View'

//** utils
import cx from 'classnames'
import {useAppDispatch} from '../../hooks/useAppDispatch'
import {getAllowedCodesAmount, sortingCodesHandler} from '../../helpers'
import {toast} from 'react-toastify'
import {userSelectors} from '../../redux/user'
import {subscribesActions, subscribesSelectors} from '../../redux/subscribes'
import {cardsActions} from '../../redux/cards'
import {codesActions} from '../../redux/codes'

let _subsGapWidth = 60 /** gap between sub elements => look at .subscription class **/
let _startPosition = 60 /** start position of the first slide = -60px **/

export default function Subscriptions() {

  const isAuth = useSelector(userSelectors.isAuth)
  const subscribes = useSelector(subscribesSelectors.subscribes)
  const noSubs = subscribes.length < 1

  const dispatch = useAppDispatch()
  const router = useRouter()
  const {push} = router

  const [activeSlide, setActiveSlide] = useState(0)
  const [disablePrev, setDisablePrev] = useState(true)
  const [disableNext, setDisableNext] = useState(false)

  const isUpgrading = !!localStorage.getItem('upgrading')

  const [showModal, setShowModal] = useState(false)
  const showModalHandler = () => {
    setShowModal(!showModal)
  }
  const licenseRef = useRef<HTMLDivElement>(null)
  const scrollToCodesHandler = () => {
    window.scrollTo({
      top: 636, /** Y coordinate of licenses **/
      behavior: 'smooth'
    })
  }

  useEffect(() => {
    showModal
      ? document.body.setAttribute('style', 'overflow: hidden;')
      : document.body.setAttribute('style', 'overflow: auto;')

  }, [showModal])

  useEffect(() => {
    dispatch(subscribesActions.subscribesGet())
  }, [dispatch])

  /** SORTING ACTIVE SUB'S CODES **/
  const activeSlideCodes = subscribes[activeSlide]?.codes
  const sortedCodes = sortingCodesHandler(activeSlideCodes)
  /** END OF SORTING **/

  /** SOLVING AMOUNT OF ALLOWED CODES TO CONFIRM **/
  const hasHolds = activeSlideCodes?.some((code: ICode) => code.status === STATUSES.HOLD)
  const checkedCodes = sortedCodes.filter((code: ICode) => code.checked)
  const allowedCodesAmount = getAllowedCodesAmount(subscribes[activeSlide]?.product)
  /** END OF SOLVING **/

  /** SUBSCRIPTIONS SLIDER **/
  let initWindowWidth = window.innerWidth
  const subRef = useRef<HTMLDivElement>(null)

  useEffect(() => {
    if (initWindowWidth < 769) {
      _subsGapWidth = 32
      _startPosition = 16
    } else {
      _subsGapWidth = 60
      _startPosition = 60
    }
  }, [initWindowWidth])

  const setSubWidthHandler = useCallback((event: UIEvent) => {
    let newWindowWidth = (event.currentTarget as Window).innerWidth

    if (initWindowWidth !== newWindowWidth) initWindowWidth = window.innerWidth
  }, [])

  useEffect(() => {
    window.addEventListener('resize', (e) => setSubWidthHandler(e))

    return window.removeEventListener('resize', (e) => setSubWidthHandler(e))
  }, [setSubWidthHandler])

  const subPositionHandler = useCallback((event: Event) => {
    const child = (event.currentTarget as HTMLDivElement).firstElementChild
    if (child) {
      const firstSubPosition = child.getBoundingClientRect().x
      const subWidth = child.clientWidth
      const activeSlideIndex =
        Math.abs(Math.floor((firstSubPosition - _startPosition) / (subWidth + _subsGapWidth)))

      setActiveSlide(activeSlideIndex)
      setDisablePrev(activeSlideIndex === 0)
      setDisableNext(subscribes.length - 1 <= activeSlideIndex )
    }
  }, [subscribes.length])

  const nextSubHandler = () => {
    const child = (subRef.current as HTMLDivElement)
    const sub = child.firstElementChild
    const subWidth = (sub && sub.clientWidth + _subsGapWidth) || 0

    if (child) {
      child.scrollTo({
        left: subWidth * (activeSlide + 1),
        behavior: 'smooth'
      })
    }
  }

  const prevSubHandler = () => {
    const child = (subRef.current as HTMLDivElement)
    const sub = child.firstElementChild
    const subWidth = (sub && sub.clientWidth) || 0

    if (child) {
      child.scrollTo({
        left: (subWidth + _subsGapWidth) * (activeSlide - 1),
        behavior: 'smooth'
      })
    }
  }

  useEffect(() => {
    subRef.current?.addEventListener('scroll', (e) => subPositionHandler(e))

    return subRef.current?.removeEventListener('scroll', (e) => subPositionHandler(e))
  }, [subPositionHandler])
  /** END OF SLIDER **/

  const upgradeHandler = () => {
    const payload = {
      id: subscribes[activeSlide].productId
    }
    dispatch(cardsActions.setCard(payload))
    dispatch(subscribesActions.setToUpgrade(subscribes[activeSlide]))
    localStorage.setItem('upgrading', 'true')
    push(ROUTES.ROOT)
  }

  const confirmDomains = (checkedCodes: ICode[], subscribeId: number) => {
    let codes: number[] = []
    checkedCodes.map(item => codes.push(item.id))
    dispatch(codesActions.domainsConfirm({codesIds: codes, subscribeId}))
      .then(res => {
        if (typeof res.payload === 'string' && res.payload.startsWith('Error')) {
          toast.error(res.payload)
        } else {
          dispatch(subscribesActions.subscribesGet())
          toast.success('Successful!')
        }
      })
    localStorage.removeItem('upgrading')
    dispatch(subscribesActions.allCheckedClear())
  }

  if (!isAuth) {
    push(ROUTES.ROOT)
    return <Redirecting />
  }

  if (noSubs) {
    return (
      <MainLayout title="Subscriptions | G-score">
        <div className={classes.headerLine}>
          <h1 className={classes.title}>My subscriptions</h1>
        </div>
        <NoSubs />
      </MainLayout>
    )
  }

  return (
    <MainLayout title="Subscriptions | G-score">
      <div className={classes.headerLine}>
        <h1 className={classes.title}>My subscriptions</h1>
        <Button
          title="Upgrade"
          theme="red"
          cWidth="short"
          onClick={upgradeHandler}
        />
        <span className={classes.responsiveUpgrade} onClick={upgradeHandler}>
          Upgrade
        </span>
      </div>

      <div className={classes.subscriptions} ref={subRef}>
        {subscribes && subscribes.map((item: ISubscribeItem, index: number) => {

          if (subscribes.length === index + 1) {
            return (
              <Fragment key={1111}>
                <Sub
                  key={item.id}
                  subItem={item}
                  activeSlide={index === activeSlide}
                  modalHandler={showModalHandler}
                  scrollToCodesHandler={scrollToCodesHandler}
                />
                <HollowSub />
                <HollowSub />
              </Fragment>
            )
          }

          return (
            <Sub
              key={item.id}
              subItem={item}
              activeSlide={index === activeSlide}
              modalHandler={showModalHandler}
              scrollToCodesHandler={scrollToCodesHandler}
            />
          )
        })}
      </div>

      <Pagination
        disablePrev={disablePrev}
        disableNext={disableNext}
        activeSub={activeSlide + 1}
        subsLength={subscribes.length}
        nextSubHandler={nextSubHandler}
        prevSubHandler={prevSubHandler}
      />

      <div
        ref={licenseRef}
        className={cx(classes.licenses, {[classes.noSelectDomains]: !hasHolds})}
      >
        {subscribes && sortedCodes.map((license: ICode) => {
          return (
            <License
              key={license.id}
              license={license}
              codeId={license.id}
              subscribeId={subscribes[activeSlide].id}
              disable={checkedCodes.length >= allowedCodesAmount! || !isUpgrading}
            />
          )
        })}
      </div>

      {hasHolds &&
        <div className={classes.selectDomains}>
          <span>Select the domains you want to keep</span>
          <Button
            title="Confirm"
            theme={checkedCodes.length !== allowedCodesAmount ? 'disable' : 'red'}
            cWidth="short"
            onClick={() => confirmDomains(checkedCodes, subscribes[activeSlide].id)}
          />
        </div>
      }

      {showModal &&
        <View modalHandler={showModalHandler}>
          {subscribes && sortedCodes.map((license: ICode) => {
            return (
              <License
                key={license.id}
                license={license}
                codeId={license.id}
                subscribeId={subscribes[activeSlide].id}
                disable={checkedCodes.length >= allowedCodesAmount! || !isUpgrading}
              />
            )
          })}
        </View>
      }
    </MainLayout>
  )
}
