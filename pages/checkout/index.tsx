import {useState} from 'react'
import {useSelector} from 'react-redux'
import {useRouter} from 'next/router'
import {ROUTES} from '../../constants'

//** components
import {WizardContainer} from '../../layout/_components/WizardContainer'
import {Tabs} from '../../layout/Tabs'
import {CheckoutForm} from '../../layout/CheckoutForm'
import {MainLayout} from '../../layout/MainLayout'
import {Redirecting} from '../../layout/Redirecting'

//** utils
import {cardsSelectors} from '../../redux/cards'
import {userSelectors} from '../../redux/user'

export default function Checkout() {

  const isAuth = useSelector(userSelectors.isAuth)
  const selectedCard = useSelector(cardsSelectors.selectedCard)

  const router = useRouter()
  const {push} = router

  const [isCheckingOut, setIsCheckingOut] = useState(true)

  if (!isAuth) {
    push(ROUTES.ROOT)
    return <Redirecting />
  }

  return (
    <MainLayout title="Checkout page">
      <WizardContainer>
        {isCheckingOut && <Tabs />}

        {selectedCard &&
          <CheckoutForm
            card={selectedCard}
            checkingOut={isCheckingOut}
            setCheckingOut={setIsCheckingOut}
          />
        }
      </WizardContainer>
    </MainLayout>
  )
}
