import classes from './index.module.scss'

//** components
import {MainLayout} from '../../layout/MainLayout'
import {WizardContainer} from '../../layout/_components/WizardContainer'
import {Tabs} from '../../layout/Tabs'
import {AuthForm} from '../../layout/AuthForm'

export default function SignIn() {
  return (
    <MainLayout title="Sign-in | G-score">
      <WizardContainer>
        <Tabs />
        <div className={classes.formContainer}>
          <h1>Log in</h1>
          <AuthForm />
        </div>
      </WizardContainer>
    </MainLayout>
  )
}

export async function getStaticProps() {
  return {
    props: {},
  }
}
