import facebook from './Facebook.svg'
import linkedIn from './LinkedIn.svg'
import twitter from './Twitter.svg'

import checkMark from './checkmark.svg'
import settings from './settings.svg'
import logout from './logout.svg'
import graBasket from './grayBasket.svg'
import arrow from './arrow.svg'
import checkCircle from './checkCircle.svg'
import checkCircleRed from './checkCircleRed.svg'
import copy from './copy.svg'
import close from './close.svg'
import menuIcon from './menuIcon.svg'
import chevron from './chevron.svg'

import checkbox from './initCheck.svg'
import disable from './disableCheck.svg'
import checked from './checked.svg'
import checkedDisable from './disableCheck.svg'

export const icons = [
  {icon: facebook, link: 'https://ru-ru.facebook.com/'},
  {icon: twitter, link: 'https://twitter.com'},
  {icon: linkedIn, link: 'https://ru.linkedin.com/'},
]

export const checkboxes = {
  checkbox,
  disable,
  checked,
  checkedDisable,
}

export const imgKit = {
  checkMark,
  settings,
  logout,
  graBasket,
  arrow,
  checkCircle,
  checkCircleRed,
  copy,
  close,
  menuIcon,
  chevron,
}
