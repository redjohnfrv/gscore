import React from 'react'
import {useSelector} from 'react-redux'
import classes from './PasswordForm.module.scss'

//** components
import {Field, Form} from 'react-final-form'
import {Input} from '../_components/Input'
import {Button} from '../_components/Button'

//** utils
import {userActions, userSelectors} from '../../redux/user'
import {composeValidators, validators} from '../../helpers'
import {useAppDispatch} from '../../hooks/useAppDispatch'
import {toast} from 'react-toastify'

interface IFormValue extends FormData {
  currentPassword: string
  newPassword: string
}

export const PasswordForm = () => {

  const dispatch = useAppDispatch()
  const isRequesting = useSelector(userSelectors.isLoading)

  const onSubmit = (value: IFormValue) => {
    const {currentPassword, newPassword} = value
    dispatch(userActions.changePassword({currentPassword, newPassword}))
      .then((res: any) => {
          res.error
            ? toast.error(res.payload)
            : toast.success('Password successful changed!')
        }
      )
  }

  return (
    <Form
      onSubmit={onSubmit}
      render={({
       handleSubmit,
       invalid
      }) => (
        <form onSubmit={handleSubmit} className={classes.form}>
          <Field
            type="password"
            name="currentPassword"
            placeholder="Current password"
            component={Input}
            validate={composeValidators(validators.required, validators.passwordLength)}
          />
          <Field
            type="password"
            name="newPassword"
            placeholder="New password"
            component={Input}
            validate={composeValidators(validators.required, validators.passwordLength)}
          />
          <Button
            type="submit"
            title={isRequesting ? "Processing ..." : "Save"}
            theme={(invalid || isRequesting) ? "disable" : "red"}
            cWidth="short"
          />
        </form>
      )}
    />
  )
}
