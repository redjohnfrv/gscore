import React from 'react'
import {FieldRenderProps} from 'react-final-form'
import classes from './Input.module.scss'

type InputFiledProps = {
  placeholder?: string
  label?: string
  disabled?: boolean
}

type Props = FieldRenderProps<string> & InputFiledProps

export const Input = ({input, meta, placeholder, label, disabled}: Props) => {
  return (
    <div className={classes.inputWrapper}>
      {label && <span>{label}</span>}
      <input
        {...input}
        disabled={disabled}
        placeholder={placeholder}
        className={classes.input}
      />
      {meta.error && meta.touched && <span className={classes.error}>{meta.error}</span>}
    </div>
    )
}
