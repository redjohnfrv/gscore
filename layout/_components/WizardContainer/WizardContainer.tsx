import React, {ReactNode} from 'react'
import classes from './WizardContainer.module.scss'

interface Props {
  children: ReactNode
}

export const WizardContainer = ({children}: Props) => {
  return (
    <div className={classes.container}>
      {children}
    </div>
  )
}
