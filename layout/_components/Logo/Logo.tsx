import React from 'react'
import {images} from '../../../assets/img/images'
import classes from './Logo.module.scss'

//** components
import Image from 'next/image'

export const Logo = () => {
  return (
    <div className={classes.logo}>
      <Image src={images.logo} alt="g-score logo" />
      <span className={classes.logoText}>g-score</span>
    </div>
  )
}
