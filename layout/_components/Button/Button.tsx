import React from 'react'
import classes from './Button.module.scss'

//** utils
import cx from 'classnames'

interface Props {
  onClick?: () => void
  title: string,
  theme?: 'white' | 'red' | 'disable',
  cWidth?: 'long' | 'medium' | 'short' | 'full',
  type?: "button" | "submit" | "reset" | undefined,
}

export const Button = ({
  onClick,
  title,
  theme = 'white',
  cWidth = 'medium',
  type = undefined,
}: Props) => {

  const buttonStyles = cx(
    {
      [classes.themeWhite]: theme === 'white',
      [classes.themeRed]: theme === 'red',
      [classes.themeDisable]: theme === 'disable',
      [classes.buttonFull]: cWidth === 'full',
      [classes.buttonLong]: cWidth === 'long',
      [classes.buttonMedium]: cWidth === 'medium',
      [classes.buttonShort]: cWidth === 'short',
    }
  )

  return (
    <button
      type={type}
      onClick={onClick}
      className={cx(classes.button, buttonStyles)}
    >
      {title}
    </button>
  )
}
