import React from 'react'
import classes from './Checkbox.module.scss'
import {checkboxes} from '../../../assets/img/icons'

//** components
import Image from 'next/image'

//** utils
import {useAppDispatch} from '../../../hooks/useAppDispatch'
import {subscribesActions} from '../../../redux/subscribes'
import cx from 'classnames'

interface Props {
  id: number
  checked?: boolean
  disable?: boolean
  subscribeId: number
}

export const Checkbox = ({id, checked = false, disable = false, subscribeId}: Props) => {

  const dispatch = useAppDispatch()

  const checkboxClickHandler = () => {
    dispatch(subscribesActions.checkCode({codeId: id, subscribeId}))
  }

  return (
    <>
      <input
        id={String(id)}
        type="checkbox"
        className={cx(classes.input, {[classes.disable]: disable && !checked})}
      />
      <label
        htmlFor={String(id)}
        className={cx(classes.label, {[classes.disable]: disable && !checked})}
        onClick={checkboxClickHandler}
      >
        {!checked && !disable && <Image src={checkboxes.checkbox} alt="checkbox" />}
        {checked && <Image src={checkboxes.checked} alt="checkbox" />}
        {disable && !checked && <Image src={checkboxes.disable} alt="checkbox" />}
      </label>
    </>
  )
}
