import React, {ReactNode} from 'react'
import classes from './Modal.module.scss'
import {imgKit} from '../../../assets/img/icons'

//** components
import Image from 'next/image'

interface Props {
  children: ReactNode
  closeHandler: () => void
}

export const Modal = ({children, closeHandler}: Props) => {
  return (
    <div className={classes.outer}>
      <div className={classes.modal}>
        {children}
        <div className={classes.close} onClick={closeHandler}>
          <Image src={imgKit.close} alt="close modal" />
        </div>
      </div>
    </div>
  )
}
