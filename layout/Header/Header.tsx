import React from 'react'
import classes from './Header.module.scss'
import {ROUTES} from '../../constants'

//** components
import {Logo} from '../_components/Logo'
import {UserMenu} from '../UserMenu'
import Link from 'next/link'

interface Props {
  isAuth: boolean
}


export const Header = ({isAuth}: Props) => {
  return (
    <div className={classes.header}>
      <Link href={ROUTES.ROOT}><a><Logo /></a></Link>

      {isAuth && <UserMenu />}
    </div>
  )
}
