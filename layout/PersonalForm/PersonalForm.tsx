import React from 'react'
import {useSelector} from 'react-redux'
import classes from './PersonalForm.module.scss'

//** components
import {Field, Form} from 'react-final-form'
import {Input} from '../_components/Input'
import {Button} from '../_components/Button'

//** utils
import {userActions, userSelectors} from '../../redux/user'
import {composeValidators, validators} from '../../helpers'
import {useAppDispatch} from '../../hooks/useAppDispatch'
import {toast} from 'react-toastify'

interface IFormValue extends FormData {
  email: string
  username: string
}

interface IUserInfo {
  email: string
  username: string
}

interface Props {
  userInfo: IUserInfo
}

export const PersonalForm = ({userInfo}: Props) => {

  const {email, username} = userInfo

  const dispatch = useAppDispatch()
  const isRequesting = useSelector(userSelectors.isLoading)

  const onSubmit = (value: IFormValue) => {
    const {email, username} = value
    dispatch(userActions.changeInfo({email, username}))
      .then((res: any) => {
        res.error
          ? toast.error(res.payload)
          : toast.success('Changes successful applied!')
        }
      )
  }

  return (
    <Form
      onSubmit={onSubmit}
      render={({handleSubmit, invalid}) => (
        <form onSubmit={handleSubmit} className={classes.form}>
          <Field
            name="username"
            placeholder={username}
            component={Input}
          />
          <Field
            name="email"
            placeholder={email}
            component={Input}
            validate={composeValidators(validators.required, validators.validEmail)}
          />
          <Button
            type="submit"
            title={isRequesting ? "Processing ..." : "Save"}
            theme={(invalid || isRequesting) ? "disable" : "red"}
            cWidth="short"
          />
        </form>
      )}
    />
  )
}
