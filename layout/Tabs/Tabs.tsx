import React from 'react'
import {useRouter} from 'next/router'
import classes from './Tabs.module.scss'

//** utils
import cx from 'classnames'

export const Tabs = () => {

  const router = useRouter()
  const {pathname} = router

  const logInTabActive = pathname === '/sign-in' || pathname === '/checkout'
  const checkoutTabActive = pathname === '/checkout'

  return (
    <nav className={classes.tabs}>
      <div className={cx(classes.tab, classes.tabActive)}>Create account</div>
      <div className={cx(classes.tab, {[classes.tabActive]: logInTabActive})}>Log in</div>
      <div className={cx(classes.tab, {[classes.tabActive]: checkoutTabActive})}>Checkout</div>
    </nav>
  )
}
