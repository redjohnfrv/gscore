import React, {useCallback, useState} from 'react'
import {useSelector} from 'react-redux'
import Router from 'next/router'
import classes from './Card.module.scss'
import {ICard} from '../../dto/card'
import {imgKit} from '../../assets/img/icons'
import {ROUTES} from '../../constants'

//** components
import Image from 'next/image'
import {Button} from '../_components/Button'

//** utils
import {pluralizer} from '../../helpers'
import {useAppDispatch} from '../../hooks/useAppDispatch'
import {cardsActions} from '../../redux/cards'
import {subscribesActions, subscribesSelectors} from '../../redux/subscribes'
import {toast} from 'react-toastify'
import cx from 'classnames'

interface Props extends ICard {
  auth?: boolean
}

export const Card = ({id, prices, sitesCount, auth = false}: Props) => {

  const {push} = Router
  const dispatch = useAppDispatch()
  const subToUpgrade = useSelector(subscribesSelectors.subToUpgrade)
  const [isHover, setIsHover] = useState(false)
  const checkMarkImage = isHover ? imgKit.checkCircleRed : imgKit.checkCircle

  const onMouseEnterCallback = useCallback(() => setIsHover(true), [])
  const onMouseLeaveCallback = useCallback(() => setIsHover(false), [])

  const getGScoreHandler = (id: number) => {
    const payload = {
      id,
    }

    dispatch(cardsActions.getCards()).then(() => {
      dispatch(cardsActions.setCard(payload))
    }).catch(err => toast.error(err))

    auth
      ? push('/checkout')
      : push('/sign-up')
  }

  const upgradeProduct = () => {
    if (subToUpgrade) {
      const {id: subscribeId} = subToUpgrade
      dispatch(subscribesActions.subscribeChange({productId: id, subscribeId}))
        .then(res => {
          typeof res.payload === 'string' && res.payload.startsWith('Error')
            ? toast.error(res.payload)
            : push(ROUTES.SUBSCRIPTIONS)
        })
    }
  }

  return (
    <div
      className={cx(classes.card, {[classes.cardHover]: isHover})}
      onMouseEnter={onMouseEnterCallback}
      onMouseLeave={onMouseLeaveCallback}
    >
      <div className={classes.head}>
        <span className={classes.price}>
          {prices[0].price.startsWith('network') ? '' : '$'}
          {prices[0].price}
        </span>
        <span className={classes.license}>{pluralizer(sitesCount)} site licence</span>
        <span className={classes.description}>
          Get the advanced WordPress plugin that optimizes content with GSC keywords at one low annual price
        </span>
      </div>
      <div className={classes.checkMarks}>
        <div className={classes.mark}>
          <Image src={checkMarkImage} alt="features" />
          {sitesCount === 1
            ? <span>Single site license</span>
            : <span>All features for {sitesCount} sites</span>
          }
        </div>
        <div className={classes.mark}>
          <Image src={checkMarkImage} alt="features" />
          <span>Special introductory pricing</span>
        </div>
        <div className={classes.mark}>
          <Image src={checkMarkImage} alt="features" />
          <span>Unlimited Pages and Keywords</span>
        </div>
        <div className={classes.mark}>
          <Image src={checkMarkImage} alt="features" />
          <span>Billed annually</span>
        </div>
      </div>
      <div className={classes.buttonWrapper}>
        <Button
          title="Get Gscore"
          onClick={localStorage.getItem('upgrading')
            ? () => upgradeProduct()
            : () => getGScoreHandler(id)
          }
        />
      </div>
    </div>
  )
}
