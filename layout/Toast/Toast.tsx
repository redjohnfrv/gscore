import React from 'react'
import 'react-toastify/dist/ReactToastify.css'
import {ToastContainer} from 'react-toastify'

export const Toast = () => {
  return (
    <ToastContainer
      autoClose={6000}
      hideProgressBar={true}
      draggablePercent={60}
      newestOnTop={true}
      position="top-right"
    />
  )
}
