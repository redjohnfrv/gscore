import React from 'react'
import {useSelector} from 'react-redux'
import {useRouter} from 'next/router'
import classes from './AuthForm.module.scss'
import {ROUTES} from '../../constants'

//** components
import {Field, Form} from 'react-final-form'
import {Input} from '../_components/Input'
import {Button} from '../_components/Button'

//** utils
import {userActions, userSelectors} from '../../redux/user'
import {useAppDispatch} from '../../hooks/useAppDispatch'
import {toast} from 'react-toastify'
import {composeValidators, validators} from '../../helpers'

interface IFormValues {
  email: string,
  password: string,
  username?: string
}

interface Props {
  isSignUp?: boolean
}

export const AuthForm = ({isSignUp = false}: Props) => {

  const dispatch = useAppDispatch()
  const router = useRouter()
  const {push} = router
  const isRequesting = useSelector(userSelectors.isLoading)

  function onSubmit(values: IFormValues) {
    const payload = {
      email: values.email,
      password: values.password,
      username: values.username,
    }
    try {
      isSignUp
        ? dispatch(userActions.signupUser(payload)).then((res: any) => {
          if (res.error) {
            toast.error(res.payload)
          } else {
            toast.success('Registration complete!')
            push(ROUTES.SIGN_IN)
          }
        })
        : dispatch(userActions.loginUser(payload)).then((res: any) => {
            if (res.error) {
              toast.error(res.payload)
            } else {
              push(ROUTES.CHECKOUT)
            }
        })
    } catch (err: any) {
      toast.error(err)
    }
  }

  return (
    <Form
      onSubmit={onSubmit}
      render={({handleSubmit, invalid}) => (
        <form onSubmit={handleSubmit} className={classes.form}>
          {isSignUp && <Field
            name="username"
            placeholder="Username"
            component={Input}
          />}
          <Field
            name="email"
            placeholder="Email"
            component={Input}
            validate={composeValidators(validators.required, validators.validEmail)}
          />
          <Field
            type="password"
            name="password"
            placeholder="Password"
            component={Input}
            validate={composeValidators(validators.required, validators.passwordLength)}
          />
          {isSignUp ? (
              <Button
                type="submit"
                title={isRequesting ? "Processing ..." : "Register"}
                theme={(invalid || isRequesting) ? "disable" : "red"}
                cWidth="medium"
              />
            ) : (
              <Button
                type="submit"
                title={isRequesting ? "Processing ..." : "Log in"}
                theme={(invalid || isRequesting) ? "disable" : "red"}
                cWidth="medium"
              />
          )}

        </form>
      )}
    />
  )
}
