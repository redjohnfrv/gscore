import React from 'react'
import {useSelector} from 'react-redux'
import {useRouter} from 'next/router'
import classes from './CheckoutForm.module.scss'
import {imgKit} from '../../assets/img/icons'
import {ICard} from '../../dto/card'
import {ROUTES} from '../../constants'

//** components
import Image from 'next/image'
import {Button} from '../_components/Button'

//** utils
import cx from 'classnames'
import {useAppDispatch} from '../../hooks/useAppDispatch'
import {paymentsActions, paymentsSelectors} from '../../redux/payments'
import {toast} from 'react-toastify'

interface Props {
  card: ICard
  checkingOut: boolean
  setCheckingOut: (bool: boolean) => void
}

export const CheckoutForm = ({card, checkingOut, setCheckingOut}: Props) => {

  const dispatch = useAppDispatch()
  const router = useRouter()
  const {push} = router
  const isRequesting = useSelector(paymentsSelectors.isLoading)

  const {name, prices} = card
  const price = prices[0].price
  const priceId = prices[0].id

  const paymentsHandler = (priceId: number) => {
    dispatch(paymentsActions.paymentsBuy(priceId))
      .then(res => {
        if (typeof res.payload === 'string' && res.payload.startsWith('Error')) {
          toast.error(res.payload)
        } else {
          toast.success('Successful purchase!')
          setCheckingOut(false)
        }
      })
  }

  return (
    <div className={cx(classes.container, {[classes.noMargin]: !checkingOut})}>
      <h1>
        {
          checkingOut
            ? 'Checkout'
            : 'Start your subscription'
        }
      </h1>

      {!checkingOut &&
        <span className={classes.successLabel}>
          We have sent you a payment receipt by e-mail and a link to download the plugin with a license key.
        </span>
      }

      <div className={cx(classes.lineList, {[classes.lineListCheckedOut]: !checkingOut})}>
        <div className={classes.line}>
          <span className={classes.lineNameTitle}>Package name</span>
          <span className={classes.linePriceTitle}>Price</span>
        </div>
        <div className={classes.line}>
          <span className={classes.lineNameValue}>{name} license</span>
          <div className={classes.linePriceValue}>
            <span>${price}</span>
            <Image
              src={imgKit.graBasket}
              alt="basket"
              className={cx({[classes.visuallyHidden]: !checkingOut})}
            />
          </div>
        </div>
      </div>

      {checkingOut &&
        <div className={classes.totalLine}>
          <span>Total:</span>
          <span>${price}</span>
        </div>
      }

      {checkingOut
        ? <Button
            title={isRequesting ? 'Processing...' : 'Purchase'}
            theme={isRequesting ? 'disable' : 'red'}
            cWidth="medium"
            onClick={() => paymentsHandler(priceId)}
          />
        : <Button
            title="Go to my subscriptions"
            theme="red"
            cWidth="full"
            onClick={() => push(ROUTES.SUBSCRIPTIONS)}
          />
      }
    </div>
  )
}
