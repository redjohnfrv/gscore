import React from 'react'
import classes from './Footer.module.scss'
import {icons} from '../../assets/img/icons'

//** components
import Image from 'next/image'
import Link from 'next/link'
import {Logo} from '../_components/Logo'

export const Footer = () => {

  return (
    <div className={classes.footer}>
      <Logo />
      <span className={classes.text}>
        Ut enim ad minim veniam quis nostrud exercitation  ea commodo
      </span>
      <div className={classes.bottomLine}>
        <div className={classes.copyright}>
          Copyright © 2022 GScore |
          All Rights Reserved |
          <Link href="/"><a> Cookies</a></Link> |
          <Link href="/"><a> Privacy Policy</a></Link>
        </div>
        <div className={classes.icons}>
          {icons.map(item => {
            return (
              <Link href={item.link} key={item.link}>
                <a target="_blank">
                  <Image src={item.icon} alt={item.link} />
                </a>
              </Link>
            )
          })}
        </div>
      </div>
    </div>
  )
}
