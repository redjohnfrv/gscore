import React from 'react'
import {useRouter} from 'next/router'
import {useSelector} from 'react-redux'
import classes from './UserMenu.module.scss'
import {ROUTES} from '../../constants'
import {imgKit} from '../../assets/img/icons'

//** components
import Link from 'next/link'
import Image from 'next/image'
import {Logo} from '../_components/Logo'

//** utils
import {userActions, userSelectors} from '../../redux/user'
import {cardsActions, cardsSelectors} from '../../redux/cards'
import {useAppDispatch} from '../../hooks/useAppDispatch'
import {useMenuAnimation} from '../../hooks/useMenuAnimation'

export const UserMenu = () => {

  const router = useRouter()
  const {push, pathname} = router
  const dispatch = useAppDispatch()

  const user = useSelector(userSelectors.getUserData)
  const selectedCard = useSelector(cardsSelectors.selectedCard)

  /** USER MENU ANIMATIONS **/
  const userMenuChevron = useMenuAnimation({
    defaultValue: false,
    defaultClass: classes.userMenuChevron,
    actionClass: classes.userMenuChevronUp,
  })

  const userMenu = useMenuAnimation({
    defaultValue: false,
    defaultClass: classes.userMenu,
    actionClass: classes.userMenuUp,
  })

  const userMenuHandler = () => {
    userMenuChevron.toggle()
    userMenu.toggle()
  }
  /** END OF USER MENU ANIMATION **/

  const logoutHandler = () => {
    dispatch(userActions.logout())
    dispatch(cardsActions.cancelUpgrade())

    if (!mobileMenu.state)
      document.body.setAttribute('style', 'overflow: auto;')

    push(ROUTES.ROOT)
  }

  const upgradeCancel = () => {
    dispatch(cardsActions.cancelUpgrade())
    push(ROUTES.SUBSCRIPTIONS)
  }

  /** RESPONSIVE (MOBILE SCREEN) MENU ANIMATIONS **/
  const mobileMenu = useMenuAnimation({
    defaultValue: true,
    defaultClass: classes.mobileMenu,
    actionClass: classes.mobileMenuUp,
  })

  const mobileMenuClose = useMenuAnimation({
    defaultValue: true,
    defaultClass: classes.mobileMenuClose,
    actionClass: classes.mobileMenuCloseUp,
  })

  const nestedMenuList = useMenuAnimation({
    defaultValue: false,
    defaultClass: classes.nestedMenuList,
    actionClass: classes.nestedMenuListUp,
  })

  const nestedMenuChevron = useMenuAnimation({
    defaultValue: false,
    defaultClass: classes.nestedMenuChevron,
    actionClass: classes.nestedMenuChevronUp,
  })

  const mobileMenuHandler = () => {
    mobileMenu.toggle()
    mobileMenuClose.toggle()

    mobileMenu.state
      ? document.body.setAttribute('style', 'overflow: hidden;')
      : document.body.setAttribute('style', 'overflow: auto;')
  }

  const nestedMenuHandler = () => {
    nestedMenuList.toggle()
    nestedMenuChevron.toggle()
  }
  /** END OF RESPONSIVE MENU **/

  return (
    <>
      {!mobileMenu.state &&
        <div className={classes.documentDarkBg} />
      }
      {/** FULL DISPLAY USER MENU **/}
      <nav className={classes.preference}>
        {selectedCard && localStorage.getItem('upgrading') &&
          <div
            className={classes.cancelUpgrade}
            onClick={upgradeCancel}
          >
            CANCEL UPGRADE
          </div>
        }
        <Link href={ROUTES.SUBSCRIPTIONS}>
          <a
            className={pathname === ROUTES.SUBSCRIPTIONS
              ? classes.mySubscriptionsGray
              : undefined
            }
          >My subscriptions</a>
        </Link>
        <div
          className={classes.user}
          onClick={userMenuHandler}
        >
          <span>{user?.username || 'Agent 007'}</span>
          <div className={userMenuChevron.styles}>
            <Image src={imgKit.checkMark} alt="checkmark" />
          </div>

          <nav className={userMenu.styles}>
            <div className={classes.userMenuItem}>
              <Image src={imgKit.settings} alt="settings" />
              <Link href={ROUTES.SETTINGS}><a>Settings</a></Link>
            </div>
            <div className={classes.userMenuItem}>
              <Image src={imgKit.logout} alt="logout" />
              <span onClick={logoutHandler}>Logout</span>
            </div>
          </nav>

        </div>
      </nav>
      {/** END OF FULL DISPLAY **/}

      {/** RESPONSIVE (MOBILE SCREEN) MENU **/}
      {selectedCard && localStorage.getItem('upgrading') &&
        <div
          className={classes.cancelUpgradeMobile}
          onClick={upgradeCancel}
        >
          CANCEL UPGRADE
        </div>
      }
      <div
        onClick={mobileMenuHandler}
        className={classes.mobileMenuIcon}
      >
        <Image src={imgKit.menuIcon} alt="menu" />
      </div>

      <div
        className={mobileMenu.styles}
      >
        <div className={classes.mobileMenuHeader}>
          <div className={mobileMenuClose.styles}>
            <Image
              onClick={mobileMenuHandler}
              src={imgKit.close}
              alt="close menu"
            />
          </div>
          <Logo />
        </div>

        <div className={classes.mobileMenuLink}>
          <Link href={ROUTES.SUBSCRIPTIONS}>
            <a
              className={pathname === ROUTES.SUBSCRIPTIONS
                ? classes.mySubscriptionsGray
                : undefined
              }
            >My subscriptions</a>
          </Link>
        </div>

        <div className={classes.nestedMenu}>
          <div
            onClick={nestedMenuHandler}
            className={classes.nestedUserLine}
          >
            <span className={classes.nestedUserName}>{user?.username || 'Agent 007'}</span>
            <div className={nestedMenuChevron.styles}>
              <Image src={imgKit.chevron} alt="nested menu" />
            </div>
          </div>

          <nav className={nestedMenuList.styles}>
            <div className={classes.nestedMenuItem}>
              <Image src={imgKit.settings} alt="settings" />
              <Link href={ROUTES.SETTINGS}><a>Settings</a></Link>
            </div>
            <div className={classes.nestedMenuItem}>
              <Image src={imgKit.logout} alt="logout" />
              <span onClick={logoutHandler}>Logout</span>
            </div>
          </nav>

        </div>
      </div>
      {/** END OF RESPONSIVE **/}
    </>
  )
}
