import React, {ReactNode} from 'react'
import {useSelector} from 'react-redux'
import classes from './MainLayout.module.scss'

//** components
import Head from 'next/head'
import {Footer} from '../Footer'
import {Container} from '../_components/Container'
import {Header} from '../Header'

//** utils
import {userSelectors} from '../../redux/user'

interface Props {
  children: ReactNode
  title: string
}

export const MainLayout = (
  {
    children,
    title = 'G-Score App',
  }: Props) => {

  const isAuth = useSelector(userSelectors.isAuth)

  return (
    <Container>
      <Head>
        <title>{title}</title>
      </Head>
      <header>
        <Header isAuth={isAuth} />
      </header>
      <main className={classes.main}>
        {children}
      </main>
      <footer>
        <Footer />
      </footer>
    </Container>
  )
}
