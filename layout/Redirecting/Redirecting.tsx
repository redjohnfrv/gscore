import React from 'react'
import classes from './Redirecting.module.scss'

//** components
import Image from 'next/image'
import {images} from '../../assets/img/images'
import {MainLayout} from '../MainLayout'

export const Redirecting = () => {
  return (
    <MainLayout title="Redirecting ...">
      <div className={classes.redirecting}>
        <div className={classes.loader}>
          <Image src={images.logo} alt="loader" />
        </div>
      </div>
    </MainLayout>
  )
}
