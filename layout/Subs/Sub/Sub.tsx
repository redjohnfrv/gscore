import React from 'react'
import classes from './Sub.module.scss'
import {ISubscribeItem} from '../../../dto/subscribe'
import {STATUSES} from '../../../constants'

//** components
import {Button} from '../../_components/Button'

//** utils
import cx from 'classnames'
import {getDate} from '../../../helpers'

interface Props {
  subItem: ISubscribeItem
  activeSlide: boolean
  modalHandler?: () => void
  scrollToCodesHandler?: () => void
}

export const Sub = ({subItem, activeSlide, modalHandler, scrollToCodesHandler}: Props) => {

  const {
    currentPeriodEnd,
    status,
    product,
  } = subItem

  return (
    <div className={cx(classes.sub, {
      [classes.activeSub]: activeSlide
    })}>
      <div className={classes.subHead}>
        <span className={classes.subName}>GScore</span>
        <span
          className={
            cx(classes.subStatus, {
              [classes.subStatusActive]: status === STATUSES.ACTIVE,
              [classes.subStatusHold]: status === STATUSES.HOLD,
            })
        }
        >{status.toLowerCase()}</span>
      </div>

      <div className={classes.license}>
        <div className={classes.licenseInfo}>
          <span className={classes.licenseName}>
            {product.name}
          </span>
          <span className={classes.licenseValid}>
            valid until {getDate(currentPeriodEnd)}
          </span>
        </div>
        <span className={classes.licenseAmount}>
          ${product.prices[0].price}
        </span>
      </div>

      <div className={classes.viewButtons}>
        <Button
          title="View"
          theme={activeSlide ? 'white' : 'disable'}
          cWidth="short"
          onClick={modalHandler}
        />

        <Button
          title="View"
          theme={activeSlide ? 'white' : 'disable'}
          cWidth="short"
          onClick={scrollToCodesHandler}
        />
      </div>
    </div>
  )
}
