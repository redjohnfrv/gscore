import React, {ReactNode} from 'react'
import classes from './View.module.scss'

//** components
import {Modal} from '../../_components/Modal'

interface Props {
  children: ReactNode
  modalHandler: () => void
}

export const View = ({children, modalHandler}: Props) => {
  return (
    <div className={classes.view}>
      <Modal closeHandler={modalHandler}>
        {children}
      </Modal>
    </div>
  )
}
