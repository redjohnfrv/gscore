import React from 'react'
import classes from './../Sub/Sub.module.scss'

//** components
import {Button} from '../../_components/Button'

//** utils
import cx from 'classnames'

export const HollowSub = () => {

  return (
    <div className={cx(classes.sub, classes.hollow)}>
      <div className={classes.subHead}>
        <span className={classes.subName}>GScore</span>
        <span className={classes.subStatus}>status</span>
      </div>

      <div className={classes.license}>
        <div className={classes.licenseInfo}>
          <span className={classes.licenseName}>
            product name
          </span>
          <span className={classes.licenseValid}>
            valid until
          </span>
        </div>
        <span className={classes.licenseAmount}>
          prices
        </span>
      </div>

      <Button
        title="View"
        theme="white"
        cWidth="short"
      />
    </div>
  )
}
