import React from 'react'
import {useRouter} from 'next/router'
import classes from './noSubs.module.scss'
import {imgKit} from '../../../assets/img/icons'

//** components
import Image from 'next/image'
import {Button} from '../../_components/Button'
import {ROUTES} from '../../../constants'

export const NoSubs = () => {

  const router = useRouter()
  const {push} = router

  return (
    <div className={classes.noSubs}>
      <div className={classes.close}>
        <Image src={imgKit.close} alt="there are no subscriptions" />
      </div>
      <h2 className={classes.title}>No active subscriptions</h2>
      <span className={classes.text}>
        You can subscribe right now by clicking on the button below
      </span>
      <Button
        onClick={() => push(ROUTES.ROOT)}
        title="Get Score"
        theme="red"
        cWidth="short"
      />
    </div>
  )
}
