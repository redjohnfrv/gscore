import React from 'react'
import classes from './Pagination.module.scss'
import {imgKit} from '../../../assets/img/icons'

//** components
import Image from 'next/image'

//** utils
import cx from 'classnames'

interface Props {
  disablePrev: boolean
  disableNext: boolean
  activeSub: number
  subsLength: number
  nextSubHandler: () => void
  prevSubHandler: () => void
}

export const Pagination = ({
  disablePrev = true,
  disableNext = false,
  activeSub,
  subsLength,
  nextSubHandler,
  prevSubHandler,
}: Props) => {

  return (
    <div className={classes.pagination}>
      <div
        className={cx(classes.arrow, {[classes.disable]: disablePrev})}
        onClick={prevSubHandler}
      >
        <Image
          src={imgKit.arrow}
          alt="prev slide"
          className={cx(classes.arrowImg, classes.arrowPrev)}
        />
      </div>

      <div className={classes.counter}>
        <span>{activeSub <= subsLength ? activeSub : subsLength}</span>
        <span>/</span>
        <span>{subsLength}</span>
      </div>

      <div
        className={cx(classes.arrow, {[classes.disable]: disableNext})}
        onClick={nextSubHandler}
      >
        <Image
          src={imgKit.arrow}
          alt="next slide"
          className={classes.arrowImg}
        />
      </div>
    </div>
  )
}
