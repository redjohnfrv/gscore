import React from 'react'
import classes from './ViewScreen.module.scss'
import {imgKit} from '../../../../assets/img/icons'
import {STATUSES} from '../../../../constants'

//** utils
import cx from 'classnames'
import {copyToBuffer} from '../../../../helpers'

//** components
import {Button} from '../../../_components/Button'
import {Checkbox} from '../../../_components/Checkbox'
import Image from 'next/image'

interface Props {
  codeId: number
  subscribeId: number
  status: string
  origin: string | null
  checked?: boolean
  disable?: boolean
  code: string
  isBusy: boolean
  activateHandler: () => void
}

export const ViewScreen = ({...props}: Props) => {

  const {
    codeId,
    subscribeId,
    status,
    origin,
    checked,
    disable,
    code,
    isBusy,
    activateHandler
  } = props

  return (
    <div className={classes.license}>
      <div className={classes.checkbox}>
        <Checkbox
          id={codeId}
          subscribeId={subscribeId}
          checked={checked}
          disable={disable}
        />
      </div>

      <div className={classes.code}>
        <span className={classes.codeTitle}>
          License code
        </span>
        <span className={classes.codeValue}>
          {code}
        </span>
        <div className={classes.copy} onClick={() => copyToBuffer(code)}>
          <Image src={imgKit.copy} alt="copy value" title="copy" />
        </div>
      </div>

      <div className={classes.domain}>
        <span className={classes.domainTitle}>
          Domain
        </span>
        <div className={classes.domainValue}>
          <span className={classes.domainText}>
          {origin ?? ''}
        </span>
          {!origin &&
            <Button
              title="Activate"
              theme={isBusy ? 'disable' : 'white'}
              cWidth="short"
              onClick={activateHandler}
            />
          }
        </div>
      </div>

      <div className={cx(classes.status, {[classes.domainWithButton]: !origin})}>
        <span className={classes.statusTitle}>
          Status
        </span>
        <span className={cx(classes.statusValue, {
          [classes.statusHold]: status === STATUSES.HOLD,
          [classes.statusInactive]: status === STATUSES.INACTIVE,
        })}>
          {status.toLowerCase()}
        </span>
      </div>
    </div>
  )
}
