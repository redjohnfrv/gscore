import React from 'react'
import classes from './ViewMobile.module.scss'
import {imgKit} from '../../../../assets/img/icons'
import {STATUSES} from '../../../../constants'

//** utils
import cx from 'classnames'
import {copyToBuffer} from '../../../../helpers'

//** components
import {Button} from '../../../_components/Button'
import {Checkbox} from '../../../_components/Checkbox'
import Image from 'next/image'

interface Props {
  codeId: number
  subscribeId: number
  status: string
  origin: string | null
  checked?: boolean
  disable?: boolean
  code: string
  isBusy: boolean
  activateHandler: () => void
}

export const ViewMobile = ({...props}: Props) => {

  const {
    codeId,
    subscribeId,
    status,
    origin,
    checked,
    disable,
    code,
    isBusy,
    activateHandler
  } = props

  return (
    <div className={classes.license}>
      <div className={classes.statusLine}>
        <div className={classes.checkbox}>
          <Checkbox
            id={codeId}
            subscribeId={subscribeId}
            checked={checked}
            disable={disable}
          />

          <div className={cx(classes.status, {[classes.domainWithButton]: !origin})}>
            <span className={cx(classes.statusValue, {
              [classes.statusHold]: status === STATUSES.HOLD,
              [classes.statusInactive]: status === STATUSES.INACTIVE,
            })}>
              {status.toLowerCase()}
            </span>
          </div>
        </div>

        {!origin &&
          <Button
            title="Activate"
            theme={isBusy ? 'disable' : 'white'}
            cWidth="short"
            onClick={activateHandler}
          />
        }
      </div>

      <div className={cx(classes.code, classes.firstCode)}>
        <span className={classes.codeTitle}>
          License code
        </span>
        <span className={classes.codeValue}>
          {code}
        </span>
        <div className={classes.copy} onClick={() => copyToBuffer(code)}>
          <Image src={imgKit.copy} alt="copy value" title="copy" />
        </div>
      </div>

      <div className={classes.code}>
        <span className={classes.codeTitle}>
          Domain
        </span>
        <div className={classes.codeValue}>
          <span className={classes.domainText}>
          {origin ?? ''}
        </span>
        </div>
      </div>
    </div>
  )
}
