import React, {useState} from 'react'
import {useSelector} from 'react-redux'
import {ICode} from '../../../dto/subscribe'

//** components
import {ViewScreen} from './ViewScreen'
import {ViewMobile} from './ViewMobile'

//** utils
import {useAppDispatch} from '../../../hooks/useAppDispatch'
import {toast} from 'react-toastify'
import {codesActions, codesSelectors} from '../../../redux/codes'
import {subscribesActions} from '../../../redux/subscribes'

interface Props {
  license: ICode
  subscribeId: number
  codeId: number
  disable?: boolean
}

export const License = ({license, subscribeId, codeId, disable}: Props) => {

  const {code, origin, status, checked} = license

  const dispatch = useAppDispatch()
  const isRequesting = useSelector(codesSelectors.isLoading)

  const [isBusy, setIsBusy] = useState(false)

  const activateHandler = () => {
    setIsBusy(!isRequesting)
    dispatch(codesActions.codesActivate(code)).then(res => {
      if (typeof res.payload === 'string' && res.payload.startsWith('Error')) {
        toast.error(res.payload)
        setIsBusy(false)
      } else {
        toast.success('Activation successful!')
        dispatch(subscribesActions.subscribesGet())
          .then(() => {
            setIsBusy(false)
          })
          .catch(err => {
            toast.error(err)
          })
      }
    }).catch(err => {
      toast.error(err)
    })
  }

  return (
    <>
      {/** full screen size **/}
      <ViewScreen
        codeId={codeId}
        subscribeId={subscribeId}
        status={status}
        checked={checked}
        disable={disable}
        origin={origin}
        code={code}
        isBusy={isBusy}
        activateHandler={activateHandler}
      />

      {/** mobile & tab screen size **/}
      <ViewMobile
        codeId={codeId}
        subscribeId={subscribeId}
        status={status}
        checked={checked}
        disable={disable}
        origin={origin}
        code={code}
        isBusy={isBusy}
        activateHandler={activateHandler}
      />
    </>
  )
}
