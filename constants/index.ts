export const ROUTES = {
  ROOT: '/',
  SUBSCRIPTIONS: '/subscriptions',
  CHECKOUT: '/checkout',
  SIGN_UP: '/sign-up',
  SIGN_IN: '/sign-in',
  SETTINGS: '/settings',
  SETTINGS_CHANGE: '/settings/change',
}

export const PENDING = '/pending'
export const FULFILLED = '/fulfilled'
export const REJECTED = '/rejected'

export const STATUSES = {
  ACTIVE: 'ACTIVE',
  HOLD: 'HOLD',
  INACTIVE: 'INACTIVE',
}

export const cardsMoq = [
  {
    "id": 1,
    "sitesCount": 1,
    "name": "One cite",
    "prices": [
      {
        "id": 1,
        "isActive": true,
        "productId": 1,
        "price": "network error"
      }
    ]
  },
  {
    "id": 2,
    "sitesCount": 3,
    "name": "Three cites",
    "prices": [
      {
        "id": 2,
        "isActive": true,
        "productId": 2,
        "price": "network error"
      }
    ]
  },
  {
    "id": 3,
    "sitesCount": 7,
    "name": "Seven sites",
    "prices": [
      {
        "id": 3,
        "isActive": true,
        "productId": 3,
        "price": "network error"
      }
    ]
  },
]
export const subsMoq = [
  {
    id: 1,
    name: 'G-Score',
    status: 'Active',
    license: 'Single site license',
    valid: '21.10.2022',
    price: '77',
  },
  {
    id: 2,
    name: 'G-Score',
    status: 'Active',
    license: 'Single site license',
    valid: '21.10.2022',
    price: '77',
  },
  {
    id: 3,
    name: 'G-Score',
    status: 'Hold',
    license: 'Three site license',
    valid: '29.11.2022',
    price: '199',
  },
  {
    id: 4,
    name: 'G-Score',
    status: 'Hold',
    license: 'Three site license',
    valid: '29.11.2022',
    price: '199',
  },
  {
    id: 5,
    name: 'G-Score',
    status: 'Active',
    license: 'Single site license',
    valid: '21.10.2022',
    price: '77',
  },
  {
    id: 6,
    name: 'G-Score',
    status: 'Active',
    license: 'Single site license',
    valid: '21.10.2022',
    price: '77',
  },
]
export const licensesMoq = [
  {
    id: 1,
    code: "sl=ru&tl=en&texte=%D0%D0%D0%D0%D0%D0%D0%D0",
    domain: "https://translate.google.com/?sl=rru&tl=enn&ten&tu&tl=enn&ten&te&t=%D0%D0%D0%D0%D0%D0%D0%D0",
    status: "Active",
  },
  {
    id: 2,
    code: "sl=ru&tl=en&texte=%D0%D0%D0%D0%D0%D0%D0%D0",
    domain: "https://translate.google.com/?sl=rru&tl=enn&ten&tu&tl=enn&ten&te&t=%D0%D0%D0%D0%D0%D0%D0%D0",
    status: "Active",
  },
  {
    id: 3,
    code: "sl=ru&tl=en&texte=%D0%D0%D0%D0%D0%D0%D0%D0",
    domain: null,
    status: "Inactive",
  }
]
