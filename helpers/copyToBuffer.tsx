import {toast} from 'react-toastify'

export const copyToBuffer = (data: string) => {
  navigator.clipboard.writeText(data).then(() =>
    toast.success('Сopied to clipboard')
  ).catch(() => {
    toast.error('Error!')
  })
}
