export const getDate = (data: number) => {
  if (data) {
    const date = new Date(Number(data + '000'))
    const year = date.getFullYear()
    const month = date.getMonth() < 9 ? `0` + (date.getMonth() + 1) : date.getMonth() + 1
    const day = date.getDate() < 10 ? `0` + date.getDate() : date.getDate()

    return `${day}.${month}.${year}`
  }

  return data
}
