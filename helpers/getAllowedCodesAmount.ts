import {IProduct} from '../dto/subscribe'

export const getAllowedCodesAmount = (product: IProduct) => {
  if (product) {
    const {name} = product

    return parseName(name)
  }
}

const parseName = (name: string): number => {
  if (name.indexOf('One') > -1) return 1
  if (name.indexOf('Three') > -1) return 3
  if (name.indexOf('Seven') > -1) return 7

  throw new Error('Что-то пошло не так ...')
}
