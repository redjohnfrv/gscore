const starsAmountReplacer = (number: number): string => {
  let stars = ''
  for (let i: number = 0; i < number; i ++) {
    stars += '*'
  }
  return stars
}

export const emailHiding = (email: string): string => {
  const startStr = email.substring(0, 3)
  const endStr = email.substring(email.length, email.length - 3)
  const starsAmount = email.length - 6

  return startStr + starsAmountReplacer(starsAmount) + endStr
}
