import {ICode} from '../dto/subscribe'
import {STATUSES} from '../constants'

export const sortingCodesHandler = (array: ICode[]) => {
  let sortedArray: ICode[] = []
  if (array)
    array.forEach(item => {
      item.status === STATUSES.ACTIVE
        ? sortedArray.unshift(item)
        : sortedArray.push(item)
    })

  return sortedArray
}
