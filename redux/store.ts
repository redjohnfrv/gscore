import {combineReducers, configureStore} from '@reduxjs/toolkit'
import {persistReducer, persistStore} from 'redux-persist'
import storage from 'redux-persist/lib/storage'
import cardsReducer from './cards'
import codesReducer from './codes'
import paymentsReducer from './payments'
import subscribesReducer from './subscribes'
import userReducer from './user'

const persistConfig = {
  key: 'root',
  storage,
}

const rootReducer = combineReducers({
  user: userReducer,
  cards: cardsReducer,
  payments: paymentsReducer,
  subscribes: subscribesReducer,
  codes: codesReducer,
})

export const store = configureStore({
  reducer: persistReducer(persistConfig, rootReducer),
  middleware: (getDefaultMiddleware) => getDefaultMiddleware({
    serializableCheck: false,
  })
})

export const persistor = persistStore(store)
