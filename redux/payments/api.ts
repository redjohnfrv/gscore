import axios from 'axios'
import {BASE_URL, URL_PAYMENTS_BUY} from '../../dto/api'
import {getToken} from '../../helpers'

export const paymentsApi = {
  paymentsBuyRequest,
}

function paymentsBuyRequest(priceId: number) {
  return axios({
    method: 'POST',
    url: `${BASE_URL}${URL_PAYMENTS_BUY}`,
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${getToken()}`,
    },
    data: {
      priceId,
    }
  })
}
