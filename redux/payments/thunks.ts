import {createAsyncThunk} from '@reduxjs/toolkit'
import {AxiosResponse} from 'axios'
import {PAYMENTS_BUY} from './types'
import {paymentsApi} from './api'

export const paymentsBuy = createAsyncThunk(
    PAYMENTS_BUY,
    async (priceId: number) => {
      try {
        const response: AxiosResponse = await paymentsApi.paymentsBuyRequest(priceId)
        return response.data
      } catch (error) {
        // const {response, message} = error as AxiosError
        // return response ? 'Error: ' + response.data.message : 'Error: ' + message
        return String(error)
      }
    })
