import {RootState} from '../types'

export const subscribe = (state: RootState) => state.payments.subscribe
export const isLoading = (state: RootState) => state.payments.loading
