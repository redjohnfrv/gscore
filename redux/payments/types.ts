import {ISubscribe} from '../../dto/subscribe'

export interface IPaymentsState {
  subscribe: ISubscribe
  loading: boolean

}

export const PAYMENTS = 'payments/'
export const PAYMENTS_BUY = 'payments/buy'
