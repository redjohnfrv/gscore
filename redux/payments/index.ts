import paymentsReducer from './slice'
import {paymentsActions} from './slice'
import * as paymentsSelectors from './selectors'

export {
  paymentsActions,
  paymentsSelectors,
}

export default paymentsReducer
