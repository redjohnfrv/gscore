import {AnyAction, createSlice} from '@reduxjs/toolkit'
import {FULFILLED, PENDING, REJECTED } from '../../constants'
import {paymentsBuy} from './thunks'
import {IPaymentsState, PAYMENTS, PAYMENTS_BUY} from './types'

//FILTER FUNCTIONS
function isPendingAction(action: AnyAction) {
  return action.type.startsWith(PAYMENTS) && action.type.endsWith(PENDING)
}

function isFulfilledAction(action: AnyAction) {
  return action.type.startsWith(PAYMENTS) && action.type.endsWith(FULFILLED)
}

function isFulfilledBuySubAction(action: AnyAction) {
  return action.type.startsWith(PAYMENTS_BUY) && action.type.endsWith(FULFILLED)
}

function isRejectedAction(action: AnyAction) {
  return action.type.startsWith(PAYMENTS) && action.type.endsWith(REJECTED)
}
//END OF FILTER FUNCTIONS

const initialState: IPaymentsState = {
  subscribe: {
    userId: 0,
    productId: 0,
    currentPeriodStart: 0,
    currentPeriodEnd: 0,
    status: '',
    id: 0,
  },
  loading: false,
}

const paymentsSlice = createSlice({
  name: 'payments',
  initialState,
  reducers: {},
  extraReducers: (mapBuilder) => {
    mapBuilder.addMatcher(isPendingAction, (state) => {
      state.loading = true
    })
    mapBuilder.addMatcher(isFulfilledBuySubAction, (state, action) => {
      state.subscribe = action.payload.subscribe
    })
    mapBuilder.addMatcher(isFulfilledAction, (state) => {
      state.loading = false
    })
    mapBuilder.addMatcher(isRejectedAction, (state) => {
      state.loading = false
    })
  },
})

const {actions, reducer} = paymentsSlice

export const paymentsActions = {...actions, paymentsBuy}

export default reducer
