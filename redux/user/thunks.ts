import {createAsyncThunk} from '@reduxjs/toolkit'
import {userApi} from './api'
import {AxiosError, AxiosResponse} from 'axios'
import {
  IUserRequest,
  IUser,
  IUserChangePassword,
  USER_LOGIN,
  USER_SIGNUP,
  USER_CHANGE,
} from './types'

export const loginUser = createAsyncThunk<
  {user: IUser, token: string},
  IUserRequest,
  {rejectValue: string }
  >
  (USER_LOGIN,
     async ({email, password}, {rejectWithValue}) => {
    try {
      const response: AxiosResponse = await userApi.login(email, password!)
      return response.data
    } catch (error) {
      const {response, message} = error as AxiosError
      const errorMsg = response ? response.data.message : message
      return rejectWithValue(errorMsg)
    }
  })

export const signupUser = createAsyncThunk<
  {user: IUser, token: string},
  IUserRequest,
  {rejectValue: string }
  >
  (USER_SIGNUP,
  async ({email, password, username}, {rejectWithValue}) => {
    try {
      const response: AxiosResponse = await userApi.signup(email, password!, username!)
      return response.data
    } catch (error) {
      const {response, message} = error as AxiosError
      const errorMsg = response ? response.data.message : message
      return rejectWithValue(errorMsg)
    }
  })

export const changeInfo = createAsyncThunk<
  {username: string, email: string},
  IUserRequest,
  {rejectValue: string }
  >(USER_CHANGE,
  async ({email, username}, {rejectWithValue}) => {
    try {
      const response: AxiosResponse = await userApi.changeInfoRequest(email, username!)
      return response.data
    } catch (error) {
      const {response, message} = error as AxiosError
      const errorMsg = response ? response.data.message : message
      return rejectWithValue(errorMsg)
    }
  })

export const changePassword = createAsyncThunk<
  {currentPassword: string, newPassword: string},
  IUserChangePassword,
  {rejectValue: string }
  >(USER_CHANGE,
  async ({currentPassword, newPassword}, {rejectWithValue}) => {
    try {
      const response: AxiosResponse = await userApi.changePasswordRequest(currentPassword, newPassword)
      return response.data
    } catch (error) {
      const {response, message} = error as AxiosError
      const errorMsg = response ? response.data.message : message
      return rejectWithValue(errorMsg)
    }
  })
