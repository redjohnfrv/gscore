import {AnyAction, AsyncThunk, createSlice} from '@reduxjs/toolkit'
import {
  changeInfo,
  changePassword,
  loginUser,
  signupUser
} from './thunks'
import {IUserState, IUser, USER_CHANGE} from './types'
import {IUserRequest, USER, USER_LOGIN, USER_SIGNUP} from './types'
import {FULFILLED, PENDING, REJECTED } from '../../constants'

type UserAsyncThunk = AsyncThunk<
  {user: IUser, token: string},
  IUserRequest,
  {rejectValue: string}
  >
type UserChangeAsyncThunk = AsyncThunk<
  {username: string, email: string},
  IUserRequest,
  {rejectValue: string}
  >

type PendingAction = ReturnType<UserAsyncThunk['pending']>
type FulfilledAction = ReturnType<UserAsyncThunk['fulfilled']>
type UserChangedAction = ReturnType<UserChangeAsyncThunk['fulfilled']>
type RejectedAction = ReturnType<UserAsyncThunk['rejected']>

//FILTER FUNCTIONS
function isPendingAction(action: AnyAction): action is PendingAction {
  return action.type.startsWith(USER) && action.type.endsWith(PENDING)
}

function isFulfilledAction(action: AnyAction): action is FulfilledAction {
  return action.type.startsWith(USER) && action.type.endsWith(FULFILLED)
}

function isFulfilledAuthAction(action: AnyAction): action is FulfilledAction {
  return (action.type.startsWith(USER_LOGIN)
    || action.type.startsWith(USER_SIGNUP)) && action.type.endsWith(FULFILLED)
}

function isFulfilledChangeAction(action: AnyAction): action is UserChangedAction {
  return (action.type.startsWith(USER_CHANGE)) && action.type.endsWith(FULFILLED)
}

function isRejectedAction(action: AnyAction): action is RejectedAction {
  return action.type.startsWith(USER) && action.type.endsWith(REJECTED)
}
//END OF FILTER FUNCTIONS

const initialState: IUserState = {
  user: null,
  isAuth: false,
  loading: false,
}

const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    logout: (state) => {
      state.user = null
      state.isAuth = false
      localStorage.removeItem('token')
    }
  },
  extraReducers: (mapBuilder) => {
    mapBuilder.addMatcher(isPendingAction, (state) => {
      state.loading = true
    })
    mapBuilder.addMatcher(isFulfilledAuthAction, (state, action) => {
      const {user, token} = action.payload
      state.user = user
      if (user) state.isAuth = true
      localStorage.setItem('token', token)
    })
    mapBuilder.addMatcher(isFulfilledChangeAction, (state, action) => {
      const {email, username} = action.payload
      state.user!.email = email
      state.user!.username = username
    })
    mapBuilder.addMatcher(isFulfilledAction, (state) => {
      state.loading = false
    })
    mapBuilder.addMatcher(isRejectedAction, (state) => {
      state.isAuth = false
      state.loading = false
      localStorage.removeItem('token')
    })
  },
})

const {actions, reducer} = userSlice

export const userActions = {
  ...actions,
  loginUser,
  signupUser,
  changeInfo,
  changePassword,
}

export default reducer
