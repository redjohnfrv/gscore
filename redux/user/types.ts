export interface IUserChangePassword {
  currentPassword: string
  newPassword: string
}

export interface IUserRequest {
  email: string
  password?: string
  username?: string
}

export interface IUser {
  id: string
  email: string
  username: string
}

export interface IUserState {
  user: IUser | null
  isAuth: boolean
  loading: boolean
}

export const USER_LOGIN = 'user/login'
export const USER_SIGNUP = 'user/signup'
export const USER_CHANGE = 'user/change'
export const USER = 'user/'
