import axios from 'axios'
import {
  BASE_URL,
  URL_SING_IN,
  URL_SING_UP,
  URL_USERS,
  URL_USERS_PASSWORD
} from '../../dto/api'
import {getToken} from '../../helpers'

export const userApi = {
  login,
  signup,
  changeInfoRequest,
  changePasswordRequest,
}

function login(email: string, password: string) {
  return axios({
    method: "POST",
    url: `${BASE_URL}${URL_SING_IN}`,
    data: {email, password},
  })
}

function signup(email: string, password: string, username: string) {
  return axios({
    method: "POST",
    url: `${BASE_URL}${URL_SING_UP}`,
    data: {email, password, username},
  })
}

function changeInfoRequest(email: string, username: string) {
  return axios({
    method: "PATCH",
    url: `${BASE_URL}${URL_USERS}`,
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${getToken()}`,
    },
    data: {email, username},
  })
}

function changePasswordRequest(currentPassword: string, newPassword: string) {
  return axios({
    method: "PATCH",
    url: `${BASE_URL}${URL_USERS_PASSWORD}`,
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${getToken()}`,
    },
    data: {currentPassword, newPassword},
  })
}
