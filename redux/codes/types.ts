import {ICodeActivateResponse} from '../../dto/subscribe'

export interface ICodesState {
  code: ICodeActivateResponse | {}
  loading: boolean
}

export const CODES = 'codes/'
export const CODES_GET = 'codes/get'
export const CODES_ACTIVATE = 'codes/activate'
export const CODES_MANAGE = 'codes/manage'
