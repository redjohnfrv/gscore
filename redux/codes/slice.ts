import {AnyAction, createSlice} from '@reduxjs/toolkit'
import {FULFILLED, PENDING, REJECTED } from '../../constants'
import {
  CODES,
  CODES_ACTIVATE,
  CODES_GET,
  CODES_MANAGE,
  ICodesState
} from './types'
import {codesActivate, domainsConfirm} from './thunks'

//FILTER FUNCTIONS
function isPendingAction(action: AnyAction) {
  return action.type.startsWith(CODES) && action.type.endsWith(PENDING)
}

function isFulfilledAction(action: AnyAction) {
  return action.type.startsWith(CODES) && action.type.endsWith(FULFILLED)
}

function isFulfilledCodeActivateAction(action: AnyAction) {
  return action.type.startsWith(CODES_ACTIVATE) && action.type.endsWith(FULFILLED)
}

function isFulfilledCodeGetAction(action: AnyAction) {
  return action.type.startsWith(CODES_GET) && action.type.endsWith(FULFILLED)
}

function isFulfilledDomainsConfirmAction(action: AnyAction) {
  return action.type.startsWith(CODES_MANAGE) && action.type.endsWith(FULFILLED)
}

function isRejectedAction(action: AnyAction) {
  return action.type.startsWith(CODES) && action.type.endsWith(REJECTED)
}
//END OF FILTER FUNCTIONS

const initialState: ICodesState = {
  code: {},
  loading: false,
}

const codesSlice = createSlice({
  name: 'codes',
  initialState,
  reducers: {},
  extraReducers: (mapBuilder) => {
    mapBuilder.addMatcher(isPendingAction, (state) => {
      state.loading = true
    })
    // mapBuilder.addMatcher(isFulfilledCodeActivateAction, (state) => {

    // })
    // mapBuilder.addMatcher(isFulfilledDomainsConfirmAction, (state, action) => {

    // })
    // mapBuilder.addMatcher(isFulfilledCodeGetAction, (state, action) => {

    // })
    mapBuilder.addMatcher(isFulfilledAction, (state) => {
      state.loading = false
    })
    mapBuilder.addMatcher(isRejectedAction, (state) => {
      state.loading = false
    })
  },
})

const {actions, reducer} = codesSlice

export const codesActions = {...actions, codesActivate, domainsConfirm}

export default reducer
