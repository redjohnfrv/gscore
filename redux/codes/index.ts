import codesReducer from './slice'
import {codesActions} from './slice'
import * as codesSelectors from './selectors'

export {
  codesActions,
  codesSelectors,
}

export default codesReducer
