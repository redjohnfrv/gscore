import {createAsyncThunk} from '@reduxjs/toolkit'
import {AxiosError, AxiosResponse} from 'axios'
import {CODES_ACTIVATE} from './types'
import {codesApi} from './api'

export const codesActivate = createAsyncThunk(
  CODES_ACTIVATE,
  async (code: string) => {
    try {
      const response: AxiosResponse = await codesApi.codeActivateRequest(code)
      return response.data
    } catch (error) {
      const {response, message} = error as AxiosError
      return response ? 'Error: ' + response.data.message : 'Error: ' + message
    }
  })

export const domainsConfirm = createAsyncThunk(
  CODES_ACTIVATE,
  async (data: {codesIds: number[], subscribeId: number}) => {
    const {codesIds, subscribeId} = data
    try {
      const response: AxiosResponse = await codesApi.domainsConfirmRequest(codesIds, subscribeId)
      return response.data
    } catch (error) {
      const {response, message} = error as AxiosError
      return response ? 'Error: ' + response.data.message : 'Error: ' + message
    }
  })
