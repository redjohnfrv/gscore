import axios from 'axios'
import {
  BASE_URL,
  URL_CODES_ACTIVATE,
  URL_CODES_GET,
  URL_CODES_MANAGE
} from '../../dto/api'
import {getToken} from '../../helpers'

export const codesApi = {
  codeActivateRequest,
  codesGetRequest,
  domainsConfirmRequest,
}

function codesGetRequest() {
  return axios({
    method: 'GET',
    url: `${BASE_URL}${URL_CODES_GET}`,
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${getToken()}`,
    },
  })
}

function codeActivateRequest(code: string) {
  return axios({
    method: 'POST',
    url: `${BASE_URL}${URL_CODES_ACTIVATE}`,
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${getToken()}`,
    },
    data: {
      code,
    }
  })
}

function domainsConfirmRequest(codesIds: number[], subscribeId: number) {
  return axios({
    method: 'PUT',
    url: `${BASE_URL}${URL_CODES_MANAGE}`,
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${getToken()}`,
    },
    data: {
      codesIds,
      subscribeId,
    },
  })
}
