import {RootState} from '../types'

export const isLoading = (state: RootState) => state.codes.loading
