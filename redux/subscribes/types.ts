import {ISubscribeItem} from '../../dto/subscribe'

export interface ISubscribesState {
  subscribes: ISubscribeItem[]
  loading: boolean
}

export const SUBSCRIBES = 'subscribes/'
export const SUBSCRIBES_GET = 'subscribes/get'
export const SUBSCRIBES_CHANGE = 'subscribes/change'
