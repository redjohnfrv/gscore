import {RootState} from '../types'
import {createSelector} from 'reselect'
import {ISubscribeItem} from '../../dto/subscribe'

export const subscribes = (state: RootState) => state.subscribes.subscribes
export const isLoading = (state: RootState) => state.subscribes.loading

export const codesBySubscriptionId = createSelector(
  (state: RootState, subId: number) => ({
    subscribes: subscribes(state),
    id: subId,
  }),
  ({subscribes, id}) => {
    if (id >= 0) {
      return subscribes.find((sub: ISubscribeItem) => sub.id === id).codes
    } else {
      return undefined
    }
  }
)

export const subToUpgrade = createSelector(
  (state: RootState) => ({
    subscribes: subscribes(state),
  }),
  ({subscribes}) => subscribes.find((sub: ISubscribeItem) => sub.toUpgrade === true)
)
