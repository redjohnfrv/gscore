import {createAsyncThunk} from '@reduxjs/toolkit'
import {AxiosError, AxiosResponse} from 'axios'
import {SUBSCRIBES_CHANGE, SUBSCRIBES_GET} from './types'
import {subscribesApi} from './api'

export const subscribesGet = createAsyncThunk(
  SUBSCRIBES_GET,
  async () => {
    try {
      const response: AxiosResponse = await subscribesApi.subscribesGetRequest()
      return response.data
    } catch (error) {
      const {response, message} = error as AxiosError
      return response ? 'Error: ' + response.data.message : 'Error: ' + message
    }
  }
)

export const subscribeChange = createAsyncThunk(
  SUBSCRIBES_CHANGE,
  async (data: {productId: number, subscribeId: number}) => {
    try {
      const response: AxiosResponse =
        await subscribesApi.subscribesChangeProductRequest(data.productId, data.subscribeId)

      return response.data
    } catch (error) {
      const {response, message} = error as AxiosError
      return response ? 'Error: ' + response.data.message : 'Error: ' + message
    }
  }
)
