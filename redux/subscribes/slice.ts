import {AnyAction, createSlice} from '@reduxjs/toolkit'
import {FULFILLED, PENDING, REJECTED } from '../../constants'
import {
  ISubscribesState,
  SUBSCRIBES,
  SUBSCRIBES_CHANGE,
  SUBSCRIBES_GET
} from './types'
import {subscribesGet, subscribeChange} from './thunks'

//FILTER FUNCTIONS
function isPendingAction(action: AnyAction) {
  return action.type.startsWith(SUBSCRIBES) && action.type.endsWith(PENDING)
}

function isFulfilledAction(action: AnyAction) {
  return action.type.startsWith(SUBSCRIBES) && action.type.endsWith(FULFILLED)
}

function isFulfilledSubscriptionsGetAction(action: AnyAction) {
  return action.type.startsWith(SUBSCRIBES_GET) && action.type.endsWith(FULFILLED)
}

function isFulfilledSubscribeChangeAction(action: AnyAction) {
  return action.type.startsWith(SUBSCRIBES_CHANGE) && action.type.endsWith(FULFILLED)
}

function isRejectedAction(action: AnyAction) {
  return action.type.startsWith(SUBSCRIBES) && action.type.endsWith(REJECTED)
}
//END OF FILTER FUNCTIONS

const initialState: ISubscribesState = {
  subscribes: [],
  loading: false,
}

const subscribesSlice = createSlice({
  name: 'subscribes',
  initialState,
  reducers: {
    checkCode: (state, action) => {
      const {codeId, subscribeId} = action.payload
      const sub = state.subscribes.find(sub => sub.id === subscribeId)
      sub!.codes.find(code => {
        if (code.id === codeId) code.checked = !code.checked
      })
    },
    allCheckedClear: (state) => {
      state.subscribes.map(sub => sub.codes.map(code => code.checked = false))
    },
    setToUpgrade: (state, action) => {
      state.subscribes.map(sub => sub.toUpgrade = sub.id === action.payload.id)
    }
  },
  extraReducers: (mapBuilder) => {
    mapBuilder.addMatcher(isPendingAction, (state) => {
      state.loading = true
    })
    mapBuilder.addMatcher(isFulfilledSubscriptionsGetAction, (state, action) => {
      if (typeof action.payload === 'string') {
        return undefined
      } else {
        state.subscribes = action.payload
      }
    })
    mapBuilder.addMatcher(isFulfilledSubscribeChangeAction, (state, action) => {
      if (typeof action.payload === 'string') {
        return undefined
      } else {
        state.subscribes.map(item => {
          if (item.id === action.payload.id) item = action.payload
        })
      }
    })
    mapBuilder.addMatcher(isFulfilledAction, (state) => {
      state.loading = false
    })
    mapBuilder.addMatcher(isRejectedAction, (state) => {
      state.loading = false
    })
  },
})

const {actions, reducer} = subscribesSlice

export const subscribesActions = {...actions, subscribesGet, subscribeChange}

export default reducer
