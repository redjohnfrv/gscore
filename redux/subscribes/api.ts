import axios from 'axios'
import {
  BASE_URL,
  URL_SUBSCRIBES_CHANGE,
  URL_SUBSCRIBES_GET
} from '../../dto/api'
import {getToken} from '../../helpers'

export const subscribesApi = {
  subscribesGetRequest,
  subscribesChangeProductRequest,
}

function subscribesGetRequest() {
  return axios({
    method: 'GET',
    url: `${BASE_URL}${URL_SUBSCRIBES_GET}`,
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${getToken()}`,
    },
  })
}

function subscribesChangeProductRequest(productId: number, subscribeId: number) {
  return axios({
    method: 'POST',
    url: `${BASE_URL}${URL_SUBSCRIBES_CHANGE}`,
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${getToken()}`,
    },
    data: {
      productId,
      subscribeId,
    },
  })
}
