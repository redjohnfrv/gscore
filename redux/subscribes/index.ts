import subscribesReducer from './slice'
import {subscribesActions} from './slice'
import * as subscribesSelectors from './selectors'

export {
  subscribesActions,
  subscribesSelectors,
}

export default subscribesReducer
