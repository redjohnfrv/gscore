import {ICard} from '../../dto/card'

export interface ICardsState {
  cards: ICard[]
  loading: boolean
}

export const CARDS = 'cards/'
export const GET_CARDS = 'cards/get'
