import {createAsyncThunk} from '@reduxjs/toolkit'
import {cardsApi} from './api'
import {AxiosError, AxiosResponse} from 'axios'
import {GET_CARDS} from './types'

export const getCards = createAsyncThunk(
  GET_CARDS,
  async () => {
    try {
      //@ts-ignore
      const response: AxiosResponse = await cardsApi.getCards()
      return response.data
    } catch (error) {
      const {response, message} = error as AxiosError
      return response ? response.data.message : message
    }
  })
