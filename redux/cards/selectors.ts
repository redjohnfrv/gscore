import {RootState} from '../types'
import {createSelector} from 'reselect'
import {ICard} from '../../dto/card'

export const cardsList = (state: RootState) => state.cards.cards
export const isLoading = (state: RootState) => state.cards.loading

export const selectedCard = createSelector(
  (state: RootState) => ({
    cards: cardsList(state),
  }),
  ({cards}) => cards.find((card: ICard) => card.chosen === true)
)
