import {AnyAction, createSlice} from '@reduxjs/toolkit'
import {getCards} from './thunks'
import {ICardsState} from './types'
import {CARDS, GET_CARDS} from './types'
import {FULFILLED, PENDING, REJECTED } from '../../constants'

//FILTER FUNCTIONS
function isPendingAction(action: AnyAction) {
  return action.type.startsWith(CARDS) && action.type.endsWith(PENDING)
}

function isFulfilledAction(action: AnyAction) {
  return action.type.startsWith(CARDS) && action.type.endsWith(FULFILLED)
}

function isFulfilledGetCardsAction(action: AnyAction) {
  return action.type.startsWith(GET_CARDS) && action.type.endsWith(FULFILLED)
}

function isRejectedAction(action: AnyAction) {
  return action.type.startsWith(CARDS) && action.type.endsWith(REJECTED)
}
//END OF FILTER FUNCTIONS

const initialState: ICardsState = {
  cards: [],
  loading: false,
}

const cardsSlice = createSlice({
  name: 'cards',
  initialState,
  reducers: {
    setCard: (state, actions) => {
      state.cards.map(card => card.chosen = card.id === actions.payload.id)
    },
    cancelUpgrade: (state) => {
      localStorage.removeItem('upgrading')
      state.cards.map(card => card.chosen = false)
    }
  },
  extraReducers: (mapBuilder) => {
    mapBuilder.addMatcher(isPendingAction, (state) => {
      state.loading = true
    })
    mapBuilder.addMatcher(isFulfilledGetCardsAction, (state, action) => {
      state.cards = [...action.payload]
    })
    mapBuilder.addMatcher(isFulfilledAction, (state) => {
      state.loading = false
    })
    mapBuilder.addMatcher(isRejectedAction, (state) => {
      state.loading = false
    })
  },
})

const {actions, reducer} = cardsSlice

export const cardsActions = {...actions, getCards}

export default reducer
