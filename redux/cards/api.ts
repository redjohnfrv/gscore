import axios from 'axios'
import {BASE_URL, URL_PRODUCTS} from '../../dto/api'

export const cardsApi = {
  getCards,
}

function getCards() {
  return axios({
    method: 'GET',
    url: `${BASE_URL}${URL_PRODUCTS}`,
    headers: {
      'Content-Type': 'application/json',
    }
  })
}
