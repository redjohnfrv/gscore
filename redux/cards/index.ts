import cardsReducer from './slice'
import {cardsActions} from './slice'
import * as cardsSelectors from './selectors'

export {
  cardsActions,
  cardsSelectors,
}

export default cardsReducer
