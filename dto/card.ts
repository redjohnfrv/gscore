export interface ICard {
  id: number
  sitesCount: number
  name: number | string
  prices: IPrices[]
  chosen?: boolean
}

export interface IPrices {
  id: number
  isActive: boolean
  productId: number
  price: string
}
