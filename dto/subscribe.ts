import {IPrices} from './card'

export interface ISubscribe {
  id: number
  userId: number
  productId: number
  currentPeriodStart: number
  currentPeriodEnd: number
  status: string
}

export interface IProduct {
  id: number
  sitesCount: number
  name: string
  prices: IPrices[]
}

export interface ICode {
  id: number
  code: string
  origin: string | null
  status: string
  subscribeId: number
  userId: number
  checked?: boolean
}

export interface ISubscribeItem {
  id: number
  userId: number
  productId: number
  currentPeriodStart: number
  currentPeriodEnd: number
  status: string
  product: IProduct
  codes: ICode[]
  toUpgrade?: boolean
}

interface ICodeActivateSub {
  id: number
  userId: number
  currentPeriodStart: number
  currentPeriodEnd: number
}

export interface ICodeActivateResponse {
  id: 0
  code: string
  origin: string
  status: string
  subscribeId: number
  subscribe: ICodeActivateSub
  userId: number
  user: object
}
