/**
 This hook is for animating elements such as a mobile menu or a dropdown list.
 Animation is built on dynamically changing classes.
 Hook accepts the object that contains:
   1) the initial value of the state for the element (true or false)
   2) the main class of the element (will not be changed)
   3) dynamic class (will be added or removed depending on the state)
 Hook returns:
   1) current element's state (boolean)
   2) state change actions (true, false or prev => !prev)
   3) dynamically generated list of classes

 !! Hook uses cx module
**/

import {useCallback, useMemo, useState} from 'react'
import cx from 'classnames'

interface Props {
  defaultValue: boolean
  defaultClass: string
  actionClass: string
}

export const useMenuAnimation = ({defaultValue = false, defaultClass, actionClass}: Props) => {
  const [state, setState] = useState(defaultValue)
  const styles = cx(defaultClass, {
    [actionClass]: state
  })

  const on = useCallback(() => setState(true), [])
  const off = useCallback(() => setState(false), [])
  const toggle = useCallback(() => setState(prev => !prev), [])

  return useMemo(
    () => ({
      state,
      on,
      off,
      toggle,
      styles,
    }),
    [state, off, on, toggle, styles]
  )
}
